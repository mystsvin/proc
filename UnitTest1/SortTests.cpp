#include "stdafx.h"
#include "CppUnitTest.h"
#include "program.h"
#include <iostream>
#include <fstream>

using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

// ��������� ��������� ������� �������
namespace all_shapes
{
	float rect_perim(rectangle &r);
	float circ_perim(circle &c);
	float tria_perim(triangle &t);
	void cont_init(crisper &x);
	void cont_clear(crisper &x);
	void cont_sort(crisper &x);
	void cont_in(crisper &x, ifstream &ifst);
	void cont_out(crisper &x, ofstream &ofst);
	void cont_rect_out(crisper &x, ofstream &ofst);
	void cont_add_one(crisper &x, shape *this_one);
	shape* cont_get_shape(crisper &x, int i);
	string rect_string_out(shape &s);
	string circ_string_out(shape &s);
	string tria_string_out(shape &s);
}

using namespace all_shapes;

namespace sort_Test
{
	TEST_CLASS(sortTests)
	{
	public:

		TEST_METHOD(main_Sort)
		{
			crisper *Mine = new crisper;
			cont_init(*Mine);

			crisper *Correct = new crisper;
			cont_init(*Correct);

			rectangle *new_rectangle = new rectangle();
			new_rectangle->x1 = 1; new_rectangle->y1 = 2;
			new_rectangle->x2 = 3; new_rectangle->y2 = 4;

			shape *obj_rectangle = new shape();
			obj_rectangle->key = RECTANGLE;
			obj_rectangle->obj = (void*)new_rectangle;
			obj_rectangle->perimeter = rect_perim(*new_rectangle);
			obj_rectangle->color = "Blue";
			obj_rectangle->density = 5;

			circle *new_circle = new circle();
			new_circle->r = 1; new_circle->x = 2; new_circle->y = 3;

			shape *obj_circle = new shape();
			obj_circle->key = CIRCLE;
			obj_circle->obj = (void*)new_circle;
			obj_circle->perimeter = circ_perim(*new_circle);
			obj_circle->color = "Red";
			obj_circle->density = 8;

			triangle *new_triangle = new triangle();
			new_triangle->a = 1; new_triangle->b = 2; new_triangle->c = 3;

			shape *obj_triangle = new shape();
			obj_triangle->key = TRIANGLE;
			obj_triangle->obj = (void*)new_triangle;
			obj_triangle->perimeter = tria_perim(*new_triangle);
			obj_triangle->color = "Yellow";
			obj_triangle->density = 8;

			circle *another_circle = new circle();
			another_circle->r = 4; another_circle->x = 2; another_circle->y = 3;

			shape *obj_ancircle = new shape();
			obj_ancircle->key = CIRCLE;
			obj_ancircle->obj = (void*)another_circle;
			obj_ancircle->perimeter = circ_perim(*another_circle);
			obj_ancircle->color = "Red";
			obj_ancircle->density = 8;

			cont_add_one(*Mine, obj_rectangle);
			cont_add_one(*Mine, obj_circle);
			cont_add_one(*Mine, obj_triangle);
			cont_add_one(*Mine, obj_ancircle);

			cont_add_one(*Correct, obj_triangle);
			cont_add_one(*Correct, obj_circle);
			cont_add_one(*Correct, obj_rectangle);
			cont_add_one(*Correct, obj_ancircle);

			cont_sort(*Mine);

			Assert::AreEqual(Mine->length, Correct->length);

			for (int i = 0; i < Mine->length; i++)
			{
				shape* First = cont_get_shape(*Mine, i);
				shape* Second = cont_get_shape(*Correct, i);
				string ans1, ans2;

				switch (First->key)
				{
				case RECTANGLE:	ans1 = rect_string_out(*First);	break;
				case CIRCLE:	ans1 = circ_string_out(*First);	break;
				case TRIANGLE:	ans1 = tria_string_out(*First);	break;
				}

				switch (Second->key)
				{
				case RECTANGLE:	ans2 = rect_string_out(*Second);	break;
				case CIRCLE:	ans2 = circ_string_out(*Second);	break;
				case TRIANGLE:	ans2 = tria_string_out(*Second);	break;
				}

				Assert::AreEqual(ans1, ans2);
			}
		};
	};
}