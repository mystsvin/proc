#include "stdafx.h"
#include "CppUnitTest.h"
#include "program.h"
#include <fstream>


using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

// ��������� ��������� ������� �������
namespace all_shapes
{
	float rect_perim(rectangle &r);
	float circ_perim(circle &c);
	float tria_perim(triangle &t);
	void cont_init(crisper &x);
	void cont_clear(crisper &x);
	void cont_sort(crisper &x);
	void cont_in(crisper &x, ifstream &ifst);
	void cont_out(crisper &x, ofstream &ofst);
	void cont_rect_out(crisper &x, ofstream &ofst);
	void cont_add_one(crisper &x, shape *this_one);
	shape* cont_get_shape(crisper &x, int i);
}

using namespace all_shapes;


namespace write_Tests
{
	TEST_CLASS(WriteTests)
	{
	public:
		TEST_METHOD(write_Empty)
		{
			ofstream ofst("write_Empty.txt");
			crisper *E = new crisper;
			cont_init(*E);
			cont_out(*E, ofst);

			ifstream ifst("write_Empty.txt");
			ifstream ifst_expect("write_ExpectedEmpty.txt");

			string result = "";
			string expectedResult = "";
			string s = "";

			while (getline(ifst, s)) result += s;
			while (getline(ifst_expect, s)) expectedResult += s;
			Assert::AreEqual(expectedResult, result);
		}
		TEST_METHOD(write_File)
		{
			ofstream ofst("write_Tests.txt");
			crisper *Correct = new crisper;
			cont_init(*Correct);

			rectangle *new_rectangle = new rectangle();
			new_rectangle->x1 = 1; new_rectangle->y1 = 2;
			new_rectangle->x2 = 3; new_rectangle->y2 = 4;

			shape *obj_rectangle = new shape();
			obj_rectangle->key = RECTANGLE;
			obj_rectangle->obj = (void*)new_rectangle;
			obj_rectangle->perimeter = rect_perim(*new_rectangle);
			obj_rectangle->color = "Blue";
			obj_rectangle->density = 5;

			circle *new_circle = new circle();
			new_circle->r = 1; new_circle->x = 2; new_circle->y = 3;

			shape *obj_circle = new shape();
			obj_circle->key = CIRCLE;
			obj_circle->obj = (void*)new_circle;
			obj_circle->perimeter = circ_perim(*new_circle);
			obj_circle->color = "Red";
			obj_circle->density = 8;

			triangle *new_triangle = new triangle();
			new_triangle->a = 1; new_triangle->b = 2; new_triangle->c = 3;

			shape *obj_triangle = new shape();
			obj_triangle->key = TRIANGLE;
			obj_triangle->obj = (void*)new_triangle;
			obj_triangle->perimeter = tria_perim(*new_triangle);
			obj_triangle->color = "Yellow";
			obj_triangle->density = 8;

			cont_add_one(*Correct, obj_rectangle);
			cont_add_one(*Correct, obj_circle);
			cont_add_one(*Correct, obj_triangle);
			cont_out(*Correct, ofst);

			ofst.close();

			ifstream ifst("write_Tests.txt");
			ifstream ifst_expect("write_ExpectedFile.txt");

			string result = "";
			string expectedResult = "";
			string s = "";

			while (getline(ifst, s)) result += s;
			while (getline(ifst_expect, s)) expectedResult += s;
			Assert::AreEqual(expectedResult, result);
		}
	};
}