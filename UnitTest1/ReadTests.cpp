#include "stdafx.h"
#include "CppUnitTest.h"
#include "program.h"
#include <iostream>
#include <fstream>

using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

// ��������� ��������� ������� �������
namespace all_shapes
{
	float rect_perim(rectangle &r);
	float circ_perim(circle &c);
	void cont_init(crisper &x);
	void cont_in(crisper &x, ifstream &ifst);
	void cont_add_one(crisper &x, shape *this_one);
	shape* cont_get_shape(crisper &x, int i);
	string rect_string_out(shape &s);
	string circ_string_out(shape &s);
	string tria_string_out(shape &s);
}

using namespace all_shapes;


namespace read_Test
{
	TEST_CLASS(readTests)
	{
	public:

		TEST_METHOD(read_Empty)
		{
			ifstream ifst("nothing.txt");
			crisper *E = new crisper;
			cont_init(*E);

			bool check = false;
			try
			{
				cont_in(*E, ifst);
			}
			catch (exception& exc)
			{
				string str = exc.what();
				if (str == "Read Error: Incorrect Key!")
					check = true;
			}
			Assert::AreEqual(true, check);
		};
		TEST_METHOD(read_Error)
		{
			ifstream ifst("wrong.txt");
			crisper *Wrong = new crisper;
			cont_init(*Wrong);

			bool check = false;
			try
			{
				cont_in(*Wrong, ifst);
			}
			catch (exception& exc)
			{
				string str = exc.what();
				if (str == "Read Error: Incorrect Density!")
					check = true;
			}
			Assert::AreEqual(true, check);
		};
		TEST_METHOD(read_Correct)
		{
			ifstream ifst("true.txt");
			crisper *Wrong = new crisper;
			cont_init(*Wrong);
			cont_in(*Wrong, ifst);

			crisper *Correct = new crisper;
			cont_init(*Correct);
			rectangle *new_rectangle = new rectangle();
			new_rectangle->x1 = 1; new_rectangle->y1 = 2;
			new_rectangle->x2 = 3; new_rectangle->y2 = 4;

			shape *obj_rectangle = new shape();
			obj_rectangle->key = RECTANGLE;
			obj_rectangle->obj = (void*)new_rectangle;
			obj_rectangle->perimeter = rect_perim(*new_rectangle);
			obj_rectangle->color = "Blue";
			obj_rectangle->density = 5;

			circle *new_circle = new circle();
			new_circle->r = 1; new_circle->x = 2; new_circle->y = 3;

			shape *obj_circle = new shape();
			obj_circle->key = CIRCLE;
			obj_circle->obj = (void*)new_circle;
			obj_circle->perimeter = circ_perim(*new_circle);
			obj_circle->color = "Red";
			obj_circle->density = 8;

			cont_add_one(*Correct, obj_rectangle);
			cont_add_one(*Correct, obj_circle);

			Assert::AreEqual(Correct->length, Wrong->length);

			for (int i = 0; i < 2; i++)
			{
				shape* First = cont_get_shape(*Wrong, i);
				shape* Second = cont_get_shape(*Correct, i);
				string ans1, ans2;

				switch (First->key)
				{
				case RECTANGLE:	ans1 = rect_string_out(*First);	break;
				case CIRCLE:	ans1 = circ_string_out(*First);	break;
				case TRIANGLE:	ans1 = tria_string_out(*First);	break;
				}

				switch (Second->key)
				{
				case RECTANGLE:	ans2 = rect_string_out(*Second);	break;
				case CIRCLE:	ans2 = circ_string_out(*Second);	break;
				case TRIANGLE:	ans2 = tria_string_out(*Second);	break;
				}

				Assert::AreEqual(ans1, ans2);
			}
		}
		TEST_METHOD(read_OneLine)
		{
			ifstream ifst("linear.txt");
			crisper *Wrong = new crisper;
			cont_init(*Wrong);
			cont_in(*Wrong, ifst);

			crisper *Correct = new crisper;
			cont_init(*Correct);
			rectangle *new_rectangle = new rectangle();
			new_rectangle->x1 = 1; new_rectangle->y1 = 2;
			new_rectangle->x2 = 3; new_rectangle->y2 = 4;

			shape *obj_rectangle = new shape();
			obj_rectangle->key = RECTANGLE;
			obj_rectangle->obj = (void*)new_rectangle;
			obj_rectangle->perimeter = rect_perim(*new_rectangle);
			obj_rectangle->color = "Blue";
			obj_rectangle->density = 5;

			cont_add_one(*Correct, obj_rectangle);

			Assert::AreEqual(Correct->length, Wrong->length);

			shape* First = cont_get_shape(*Correct, 0);
			shape* Second = cont_get_shape(*Correct, 0);
			string ans1, ans2;

			switch (First->key)
			{
			case RECTANGLE:	ans1 = rect_string_out(*First);	break;
			case CIRCLE:	ans1 = circ_string_out(*First);	break;
			case TRIANGLE:	ans1 = tria_string_out(*First);	break;
			}

			switch (Second->key)
			{
			case RECTANGLE:	ans2 = rect_string_out(*Second);	break;
			case CIRCLE:	ans2 = circ_string_out(*Second);	break;
			case TRIANGLE:	ans2 = tria_string_out(*Second);	break;
			}

			Assert::AreEqual(ans1, ans2);
		}
		TEST_METHOD(read_NotExist)
		{
			ifstream ifst("C:\\dontexist.txt");
			crisper *Wrong = new crisper;
			cont_init(*Wrong);

			bool check = false;
			try
			{
				cont_in(*Wrong, ifst);
			}
			catch (exception& exc)
			{
				string str = exc.what();
				if (str == "Read Error: File don't Exist!")
					check = true;
			}
			Assert::AreEqual(true, check);
		}
	};
}