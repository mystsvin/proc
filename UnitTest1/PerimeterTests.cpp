#include "stdafx.h"
#include "CppUnitTest.h"
#include "program.h"
#include <iostream>
#include <fstream>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

// ��������� ��������� ������� �������
namespace all_shapes
{
	float rect_perim(rectangle &r);
	float circ_perim(circle &c);
	float tria_perim(triangle &t);
}

using namespace all_shapes;

namespace perimeter_Test
{
	TEST_CLASS(perimeterTests)
	{
	public:

		TEST_METHOD(perimeter_Rectangle)
		{
			rectangle *new_rectangle = new rectangle();
			new_rectangle->x1 = 1; new_rectangle->y1 = 2;
			new_rectangle->x2 = 3; new_rectangle->y2 = 4;

			float perim = rect_perim(*new_rectangle);
			float expected = 8.000000;
			Assert::AreEqual(expected, perim);
		};

		TEST_METHOD(perimeter_Circle)
		{
			circle *new_circle = new circle();
			new_circle->r = 1; new_circle->x = 2; new_circle->y = 3;
			float perim = circ_perim(*new_circle);
			float expected = 2 * M_PI * 1;
			Assert::AreEqual(expected, perim);
		}
		TEST_METHOD(perimeter_Triangle)
		{
			triangle *new_triangle = new triangle();
			new_triangle->a = 1; new_triangle->b = 2; new_triangle->c = 3;
			float perim = tria_perim(*new_triangle);
			float expected = 6.000000;
			Assert::AreEqual(expected, perim);
		}
	};
}