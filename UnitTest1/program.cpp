#include "stdafx.h"
#include <fstream>
#include "program.h"
#include "string"
#include "iostream"
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

namespace all_shapes
{
	// ������� ��������� ��������������
	float rect_perim(rectangle &r)
	{
		return 2 * (abs(r.x1 - r.x2) + abs(r.y1 - r.y2));
	}

	// ���� ���������� ��������������
	rectangle* rect_in(ifstream &ifst)
	{
		rectangle *r = new rectangle;
		ifst.clear();
		if (!(ifst >> r->x1 >> r->y1 >> r->x2 >> r->y2))
			throw exception("Read Error: Incorrect Rectangle Data!");
		return r;
	}

	// ����� ���������� ��������������
	string rect_out(rectangle &r)
	{
		return "It is Rectangle: x1 = " + to_string(r.x1) + ", y1 = " + to_string(r.y1) + ", x2 = " + to_string(r.x2) + ", y2 = " + to_string(r.y2);
	}

	// ����� �������������� � ������
	string rect_string_out(shape &s)
	{
		rectangle *r = NULL;
		string ans = "";

		r = (rectangle*)(s.obj);
		ans = rect_out(*r);
		ans += ", color = " + s.color;
		ans += ", density = " + to_string(s.density) + "\n";
		ans += ", perimeter = " + to_string(s.perimeter) + "\n";
		return ans;
	}

	// ������� ��������� ����������
	float circ_perim(circle &c)
	{
		return 2 * M_PI * c.r;
	}

	// ���� ���������� ����������
	circle* circ_in(ifstream &ifst)
	{
		circle *c = new circle;
		ifst.clear();
		if (!(ifst >> c->r >> c->x >> c->y))
			throw exception("Read Error: Incorrect Circle Data!");
		if (c->r < 1) throw exception("Read Error: Incorrect Circle Data!");
		return c;
	}

	// ����� ���������� ����������
	string circ_out(circle &c)
	{
		return "It is Circle: r = " + to_string(c.r) + ", x = " + to_string(c.x) + ", y = " + to_string(c.y);
	}

	// ����� ���������� � ������
	string circ_string_out(shape &s)
	{
		circle *c = NULL;
		string ans = "";
		c = (circle*)(s.obj);
		ans = circ_out(*c);
		ans += ", color = " + s.color;
		ans += ", density = " + to_string(s.density) + "\n";
		ans += ", perimeter = " + to_string(s.perimeter) + "\n";
		return ans;
	}

	// ������� ��������� ������������
	float tria_perim(triangle &t)
	{
		return t.a + t.b + t.c;
	}

	// ���� ���������� ������������
	triangle* tria_in(ifstream &ifst)
	{
		triangle *t = new triangle;
		ifst.clear();
		if (!(ifst >> t->a >> t->b >> t->c))
			throw exception("Read Error: Incorrect Triangle Data!");
		if ((t->a < 1) || (t->b < 1) || (t->c < 1)) throw exception("Read Error: Incorrect Triangle Data!");
		return t;
	}

	// ����� ���������� ������������
	string tria_out(triangle &t)
	{
		return "It is Triangle: a = " + to_string(t.a) + ", b = " + to_string(t.b) + ", c = " + to_string(t.c);
	}

	// ����� ������������ � ������
	string tria_string_out(shape &s)
	{
		triangle *t = NULL;
		string ans = "";
		t = (triangle*)(s.obj);
		ans = tria_out(*t);
		ans += ", color = " + s.color;
		ans += ", density = " + to_string(s.density) + "\n";
		ans += ", perimeter = " + to_string(s.perimeter) + "\n";
		return ans;
	}

	// ���� ���������� ���������� ������
	shape* shape_in(ifstream &ifst)
	{
		shape *sp = new shape;
		rectangle *r = NULL;
		circle *c = NULL;
		triangle *t = NULL;
		string hh;

		int k;
		ifst.clear();
		if (!(ifst >> k))
			throw exception("Read Error: Incorrect Key!");

		switch (k)
		{
		case 1:
			r = rect_in(ifst);
			sp->key = RECTANGLE;
			sp->obj = (void*)r;
			sp->perimeter = rect_perim(*r);
			break;
		case 2:
			c = circ_in(ifst);
			sp->key = CIRCLE;
			sp->obj = (void*)c;
			sp->perimeter = circ_perim(*c);
			break;
		case 3:
			t = tria_in(ifst);
			sp->key = TRIANGLE;
			sp->obj = (void*)t;
			sp->perimeter = tria_perim(*t);
			break;
		default:
			throw exception("Read Error: Incorrect Key!");
		}

		ifst.clear();
		if (!(ifst >> sp->color))
			throw exception("Read Error: Incorrect Color!");

		ifst.clear();
		if (!(ifst >> sp->density))
			throw exception("Read Error: Incorrect Density!");
		if (sp->density < 1) throw exception("Read Error: Incorrect Density!");

		return sp;
	}

	// ����� ���������� ���������� ������
	string shape_out(shape &s)
	{
		rectangle *r = NULL;
		circle *c = NULL;
		triangle *t = NULL;
		string ans = "";

		switch (s.key)
		{
		case RECTANGLE:
			r = (rectangle*)(s.obj);
			ans = rect_out(*r);
			ans += ", color = " + s.color;
			ans += ", density = " + to_string(s.density);
			ans += ", perimeter = " + to_string(s.perimeter) + "\n";
			break;
		case CIRCLE:
			c = (circle*)(s.obj);
			ans = circ_out(*c);
			ans += ", color = " + s.color;
			ans += ", density = " + to_string(s.density);
			ans += ", perimeter = " + to_string(s.perimeter) + "\n";
			break;
		case TRIANGLE:
			t = (triangle*)(s.obj);
			ans = tria_out(*t);
			ans += ", color = " + s.color;
			ans += ", density = " + to_string(s.density);
			ans += ", perimeter = " + to_string(s.perimeter) + "\n";
			break;
		default:
			ans = "Incorrect figure!\n";
		}

		return ans;
	}

	// ������������� ����������
	void cont_init(crisper &x)
	{
		x.head = NULL;
		x.tail = NULL;
		x.length = 0;
	}

	// ���������� ��� ������������ ������
	void cont_clear(crisper &x)
	{
		while (x.length != 0)
		{
			container *tmp = x.head;
			x.head = x.head->next;
			delete tmp;
			x.length--;
		}
	}

	// ���������� ���������� �� ���������
	void cont_sort(crisper &x)
	{
		container *tmp_head = x.head; // ��������� ������
		container *temp1, *temp2;

		temp1 = tmp_head;
		temp2 = tmp_head;

		for (int in1 = 0; in1 < x.length; in1++)
		{
			for (int in2 = 0; in2 < x.length; in2++)
			{
				if (temp1->form->perimeter < temp2->form->perimeter)
				{
					shape *i = temp1->form;
					temp1->form = temp2->form;
					temp2->form = i;
				}
				temp2 = temp2->next;
				if (in2 + 1 == x.length)
					temp2 = tmp_head;
			}
			temp1 = temp1->next;
			if (in1 + 1 == x.length)
				temp1 = tmp_head;
		}
	}

	// ���������� �������� � ������
	void cont_in(crisper &x, ifstream &ifst)
	{
		if (!ifst)
			throw exception("Read Error: File don't Exist!");
		while (!ifst.eof()) {
			shape *tmp = shape_in(ifst);

			container *cont = new container;
			cont->form = tmp;

			if (cont->form != 0)
			{
				if (x.length == 0)
					x.head = x.tail = cont;
				else
				{
					x.tail->next = cont;
					cont->prev = x.tail;
					x.tail = cont;
				}

				x.length++;
			}
		}
	}

	// ����� ��������� � ����
	void cont_out(crisper &x, ofstream &ofst)
	{
		ofst << "Container contents " << x.length
			<< " elements." << endl;

		container *tmp_head = x.head;

		for (int i = 0; i < x.length; i++)
		{
			ofst << to_string(i) + ": " << shape_out(*tmp_head->form);
			tmp_head = tmp_head->next;
		}
	}

	// ����� ������ ���������������
	void cont_rect_out(crisper &x, ofstream &ofst)
	{
		ofst << "Container contents " << x.length
			<< " elements." << endl;

		container *tmp_head = x.head;

		for (int i = 0; i < x.length; i++)
		{
			if (tmp_head->form->key == RECTANGLE)
				ofst << to_string(i) + ": " << shape_out(*tmp_head->form);
			tmp_head = tmp_head->next;
		}
	}

	// ���������� ������ �������� � ���������
	void cont_add_one(crisper &x, shape *this_one)
	{
		container *cont = new container;
		cont->form = this_one;

		if (x.length == 0)
			x.head = x.tail = cont;
		else
		{
			x.tail->next = cont;
			cont->prev = x.tail;
			x.tail = cont;
		}

		x.length++;
	}

	// ����� ������ �� ����������
	shape* cont_get_shape(crisper &x, int i)
	{
		container *tmp = x.head;

		int q = 0;
		while (i != q)
		{
			q++;
			tmp = tmp->next;
		}

		return tmp->form;
	}
}