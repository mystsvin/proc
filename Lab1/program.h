#ifndef __program_atd__
#define __program_atd__
#include "string"
#include "iostream"
#include <fstream>

namespace all_shapes
{
	enum type { RECTANGLE, CIRCLE, TRIANGLE }; // ��������� ���� ����������� �����

	// ���������, ������������ �������������
	struct rectangle
	{
		int x1, y1; // ����������, ������������ ������� ����� ����
		int x2, y2; // ����������, ������������ ������ ������ ����
	};

	// ���������, ������������ ����������
	struct circle
	{
		int r; // ������ ����������
		int x, y; // ���������� ������ ����������
	};

	// ���������, ������������ �����������
	struct triangle
	{
		int a, b, c; // ������� ������������
	};

	// ���������, ���������� ��� ������
	struct shape
	{
		type key; // ����, ������������ ��� ������
		void *obj; // ������ ������
		std::string color; // ���� ������
		float density; // ��������� ���������
		float perimeter; // �������� ������
	};

	// ���������, ����������� �� ������ ���������������� ��������� ������
	struct container
	{
		shape *form; // ������
		container *next; // ��������� ������� ����������
		container *prev; // ���������� ������� ����������
	};

	// �����, ����������� ���������
	struct crisper
	{
		container *head, *tail; // ������ �� ������ � ��������� ��������
		int length; // ����� ����������
	};
};
#endif