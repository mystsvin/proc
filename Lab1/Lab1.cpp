// Lab1.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include "program.h"

using namespace std;

// c�������� ��������� ������� �������
namespace all_shapes
{
	void cont_init(crisper &x);
	void cont_clear(crisper &x);
	void cont_sort(crisper &x);
	void cont_in(crisper &x, ifstream &ifst);
	void cont_out(crisper &x, ofstream &ofst);
	void cont_rect_out(crisper &x, ofstream &ofst);
	void cont_multi_method(crisper &x, ofstream &ofst);
}

using namespace all_shapes;

int _tmain(int argc, _TCHAR* argv[])
{
	if (argc != 3)
	{
		cout << "incorrect command line! "
			"Waited: command infile outfile" << endl;
		exit(1);
	}
	ifstream ifst(argv[1]);
	ofstream ofst(argv[2]);

	cout << "Start" << endl;
	crisper *new_cris = new crisper;
	cont_init(*new_cris);

	cont_in(*new_cris, ifst);
	ofst << "Filled container." << endl;
	cont_out(*new_cris, ofst);

	ofst << "Multimethod." << endl;
	cont_multi_method(*new_cris, ofst);

	cont_sort(*new_cris);
	ofst << "Sorted container." << endl;
	cont_out(*new_cris, ofst);

	ofst << "Only rectangels." << endl;
	cont_rect_out(*new_cris, ofst);

	cont_clear(*new_cris);
	ofst << "Empty container." << endl;
	cont_out(*new_cris, ofst);

	cout << "Stop" << endl;
	return 0;
}

